#include <sawyer_application_tools/baxter_executor.h>

namespace sawyer_application_tools
{

  BaxterExecutor::BaxterExecutor() 
  {
    ROS_INFO("BaxterExecutor() created");
    ros::NodeHandle("~");
    updated_from_controller_list_ = false;
  }
  
  BaxterExecutor::~BaxterExecutor() 
  { 
    for(auto &ac: action_clients_) {
      action_clients_[ac.first].reset();
    }
  }    

  bool BaxterExecutor::setup_() 
  {
    return updateControllerInfo();
  }

  /**
   * @brief 
   */
  bool BaxterExecutor::updateControllerFromDefaultService() {

    // Setup service call
    std::string list_controllers_service = "/controller_manager/list_controllers";
    if (robot_name_!="") {
      list_controllers_service = "/" + robot_name_ + list_controllers_service;
    }
    ROS_INFO( "BaxterExecutor::setup_() -- connecting to service: %s",
	      list_controllers_service.c_str() );
    list_controllers_client_ = nh_.serviceClient<controller_manager_msgs::ListControllers>(list_controllers_service);  

    // Call service and see what controllers are available
    std::vector<std::string> controllers;
    std::vector<std::string> joints;
  
    controller_manager_msgs::ListControllers srv;
    if (list_controllers_client_.call(srv)) {
      for(auto &s: srv.response.controller) {
        if(s.claimed_resources.size()>0 && s.state=="running") {
    	  std::vector<std::string> resources;
    	  for (auto &r: s.claimed_resources)
    	    resources.insert(resources.end(),r.resources.begin(),r.resources.end());
          ROS_DEBUG("BaxterExecutor::updateControllerFromDefaultService() -- adding controller %s with %d joints", s.name.c_str(), (int)resources.size());
          controllers_[s.name] = resources;
          controller_state_[s.name] = s.state;

          std::string param_name = "/" + s.name + "/type";
          if(!robot_name_.empty()) {
            param_name = "/" + robot_name_ + param_name;
          }
          if(nh_.getParam(param_name, controller_type_[s.name])) {
            ROS_DEBUG("BaxterExecutor::updateControllerFromDefaultService() --   type %s", controller_type_[s.name].c_str()); 
          }         
	        // We'll activate the controller in the getControllerInfo function()
        }
      }
    } else {
      ROS_ERROR("BaxterExecutor::updateControllerFromDefaultService() -- failed to get controller info from controller_manager");
      return false;
    }

    return true;    
  }


  /**
   * @brief
   */
  bool BaxterExecutor::updateControllerFromControllerList() {
    std::vector<std::string> controllers;
    std::vector<std::string> joints;
    
    std::string node_name= ros::this_node::getName();
    
    if( !nh_.hasParam( node_name + "/param/controller_list" ) ) {
      ROS_INFO_STREAM("BaxterExecutor::updateControllerFromControllerList() -- No controller list specified" );
      updated_from_controller_list_ = false;
      return false;
    }
    ROS_DEBUG("[GOOD] Found controller list parameter! \n");

    XmlRpc::XmlRpcValue controller_list;
    nh_.getParam( node_name + "/param/controller_list", controller_list );
    if( controller_list.getType() != XmlRpc::XmlRpcValue::TypeArray ) {
      ROS_ERROR( "BaxterExecutor::updateControllerFromControllerList() -- Parameter controller_list should be specified as an array" );
      updated_from_controller_list_ = false;
      return false;
    }

    // Create each controller
    for( int i = 0; i < controller_list.size(); ++i ) {
      if( !controller_list[i].hasMember("name") ||
      	  !controller_list[i].hasMember("joints") ) {
	      ROS_ERROR("BaxterExecutor::updateControllerFromControllerList() -- name and joints must be specifed for each controller");
        continue;
      }
      ROS_INFO("BaxterExecutor::updateControllerFromControllerList() -- found %s in controller_list", std::string(controller_list[i]["name"]).c_str());

      try {
 
  	    std::string name = std::string(controller_list[i]["name"]);	
        std::string action_ns;
	if (controller_list[i].hasMember("action_ns")) {
          action_ns = std::string(controller_list[i]["action_ns"]);
          ROS_DEBUG("BaxterExecutor::updateControllerFromControllerList() -- action_ns = %s", action_ns.c_str());
        }
        else {
          ROS_WARN("BaxterExecutor::updateControllerFromControllerList() -- please note that 'action_ns' no longer has a default value.");
        }

        if(!action_ns.empty()) {
          name = name + "/" + action_ns;
          ROS_DEBUG("adding action ns, new name: %s", name.c_str());
        }

        if (controller_list[i]["joints"].getType() != XmlRpc::XmlRpcValue::TypeArray)
        {
          ROS_ERROR_STREAM("BasicRosControlsExecutor::updateControllerFromControllerList() -- the list of joints for controller " << name << " is not specified as an array");
          continue;
        } else {
          for(size_t idx=0; idx<controller_list[i]["joints"].size(); idx++) {
            std::string j = controller_list[i]["joints"][idx];
            ROS_DEBUG("BaxterExecutor::updateControllerFromControllerList::%s] got joint %s", name.c_str(), j.c_str());
            controllers_[name].push_back(j);
          }
        }

        if (!controller_list[i].hasMember("type"))
        {
          ROS_ERROR_STREAM("BaxterExecutor::updateControllerFromControllerList() -- no type specified for controller " << name);
          continue;
        } else {
          controller_type_[name] = std::string(controller_list[i]["type"]);
        }

        controller_state_[name] = "running"; 
      } catch (...) {
        ROS_ERROR("BaxterExecutor::updateControllerFromControllerList() -- unable to parse controller information");
      }
      
    } // end for

    updated_from_controller_list_ = true;
    return true;
  }


  
  /**
   * @brief Will try to get controllers info from controller_list, if the parameter was set
   * or from controller_manager/list_controllers service. 
   */
  bool BaxterExecutor::updateControllerInfo() 
  {
    updated_from_controller_list_ = false;
    if( !updateControllerFromControllerList() ) {
      if( !updateControllerFromDefaultService() ) {
	      ROS_INFO("BaxterExecutor::updateControllerInfo() -- Couldn't get controller info from list or default service call");
	      return false;
      }      
    }

    for( auto &c: controllers_ ) {
      setupActionClient( c.first );
    }
    return true;
  }
  

  bool BaxterExecutor::setupActionClient(std::string controller_name)
  {

    ROS_WARN("BaxterExecutor::setupActionClient() -- trying to connect to: %s. type: %s", controller_name.c_str(), controller_type_[controller_name].c_str());
    
    SimpleActionClientControllerHandleBasePtr new_handle;
    std::string action_ns = "";

    //controller_name = "/" + action_ns + "/" + controller_name + "/follow_joint_trajectory";

    if(controller_type_[controller_name].find("JointTrajectory") != std::string::npos) {

      std::string handle_name;
      if( updated_from_controller_list_ ) {
        handle_name = controller_name;
        action_ns = "";
      } else {
        if(!robot_name_.empty()) {
          handle_name = "/" + robot_name_ + "/" + controller_name;
        } else {
          handle_name = "/" + controller_name;          
        }
        action_ns = "follow_joint_trajectory";
      }
      
      new_handle.reset(new FollowJointTrajectoryControllerHandle( handle_name, action_ns));
      if (static_cast<FollowJointTrajectoryControllerHandle*>(new_handle.get())->isConnected())
      {
        ROS_INFO_STREAM("Added FollowJointTrajectory controller for " << controller_name );
        action_clients_[controller_name] = new_handle;
      }

      return true;
    }

    else if(controller_type_[controller_name].find("GripperCommand") != std::string::npos) {
      new_handle.reset(new BaxterGripperCommandControllerHandle(controller_name, action_ns));
      if (static_cast<BaxterGripperCommandControllerHandle*>(new_handle.get())->isConnected())
      {
        if (controllers_[controller_name].size() == 2)
          static_cast<BaxterGripperCommandControllerHandle*>(new_handle.get())->setParallelJawGripper(controllers_[controller_name][0], controllers_[controller_name][1]);
        }
        else
        {
          // FIXME -- what to do here?
          static_cast<BaxterGripperCommandControllerHandle*>(new_handle.get())->setCommandJoint(controllers_[controller_name][0]);
          // if (controller_list[i].hasMember("command_joint"))
          //   static_cast<GripperCommandControllerHandle*>(new_handle.get())->setCommandJoint(controller_list[i]["command_joint"]);
          // else
          //   static_cast<GripperCommandControllerHandle*>(new_handle.get())->setCommandJoint(controller_list[i]["joints"][0]);
        }

        action_clients_[controller_name] = new_handle;  
        // if (controller_list[i].hasMember("allow_failure"))
        //   static_cast<GripperCommandControllerHandle*>(new_handle.get())->allowFailure(true);
 
        //      ROS_INFO_STREAM("Added GripperCommand controller for " << controller_name );
        //      action_clients_[controller_name] = new_handle;
        //    }
        //  }


      return true;
    }

    else if(controller_type_[controller_name].find("SingleJointPosition") != std::string::npos) {
      new_handle.reset(new BaxterSingleJointPositionControllerHandle(controller_name, action_ns));
      if (static_cast<BaxterSingleJointPositionControllerHandle*>(new_handle.get())->isConnected()) {
	if( controllers_[controller_name].size() != 1 ) {
	  ROS_ERROR("For SingleJointPosition Handle, you need exactly 1 joint to be provided.");
	  return false;
	}
	static_cast<BaxterSingleJointPositionControllerHandle*>(new_handle.get())->setCommandJoint(controllers_[controller_name][0]);	  

      } else {
	return false;
      }
      
      action_clients_[controller_name] = new_handle;  

      return true;
    }

    ROS_ERROR("BaxterExecutor::setupActionClient() - don't know what to do with %s", controller_name.c_str());

    return false;

  }

  bool BaxterExecutor::sendTrajectory(const std::string &controller_name, const trajectory_msgs::JointTrajectory &trajectory)
  {
    if(action_clients_.find(controller_name) != action_clients_.end()) {
      return action_clients_[controller_name]->sendTrajectory(trajectory);
    }
    return false;
  }

  controller_manager_util::ExecutionStatus BaxterExecutor::getExecutionStatus(const std::string &controller_name)
  {

    controller_manager_util::ExecutionStatus status; 
    if(action_clients_.find(controller_name) != action_clients_.end()) {
      status = action_clients_[controller_name]->getLastExecutionStatus();
      ROS_DEBUG("BaxterExecutor::getExecutionStatus(%s) -- %s", controller_name.c_str(), status.asString().c_str());
      return status;
    }
    return controller_manager_util::ExecutionStatus::UNKNOWN;
  }


  void BaxterExecutor::preemptMotion()
  {
    //    ROS_ERROR_STREAM("BaxterExecutor::preemptMotion() -- PREEMPT!!!!");

    for(auto ac : action_clients_) {
      ac.second->cancelExecution();
    }

  }

  

}; // namespace

PLUGINLIB_EXPORT_CLASS(sawyer_application_tools::BaxterExecutor, execution_interface::ExecutionInterface)
