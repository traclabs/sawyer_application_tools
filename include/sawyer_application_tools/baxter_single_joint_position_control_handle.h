#pragma once

#include <controller_managers/simple_action_client_controller_handle.h>
#include <control_msgs/SingleJointPositionAction.h>
#include <set>

namespace simple_controller_manager
{

  /*
   * This is an interface for a head using control_msgs/SingleJointPositionAction
   * action interface (single DOF).
   */
  class BaxterSingleJointPositionControllerHandle :
    public SimpleActionClientControllerHandle<control_msgs::SingleJointPositionAction>
  {
  public:
    /* Topics will map to name/ns/goal, name/ns/result, etc */
  BaxterSingleJointPositionControllerHandle(const std::string &name, const std::string &ns) :
    SimpleActionClientControllerHandle<control_msgs::SingleJointPositionAction>(name, ns)
    {
    }
    
    bool sendTrajectory(const trajectory_msgs::JointTrajectory &trajectory)
    {
      ROS_INFO_STREAM_NAMED("BaxterSingleJointPositionController",
			    "Received new trajectory for " << name_);
      
      if (!controller_action_client_)
	return false;
      
      
      if (trajectory.points.empty())
	{
	  ROS_ERROR_NAMED("BaxterSingleJointPositionController",
			  "BaxterSingleJointPositionController requires at least one joint trajectory point.");
	  return false;
	}
      
      if (trajectory.points.size() > 1)
	{
	  ROS_INFO_STREAM_NAMED("BaxterSingleJointPositionController","Trajectory points: " << trajectory.points.size());
	}
      
      if (trajectory.joint_names.empty())
	{
	  ROS_ERROR_NAMED("BaxterSingleJointPositionController", "No joint names specified");
	  return false;
	}

      if( trajectory.joint_names.size() != 1  ) {
	ROS_ERROR_NAMED("BaxterSingleJointPositionController",
			"Joint names should be of size 1");
	return false;
      }
      
      if (command_joint_ != trajectory.joint_names[0] )
	{
	  ROS_ERROR_NAMED("BaxterSingleJointPositionController",
			  "Trajectory joint name should be same as commanded joint");
	  return false;
	}


      // Goal to be sent
      control_msgs::SingleJointPositionGoal goal;

      // Send last point
      int tpoint = trajectory.points.size() - 1;
      ROS_DEBUG_NAMED("BaxterSingleJointPositionController",
		      "Sending command from trajectory point %d",
		      tpoint);
      
      // fill in goal from last point
      if (trajectory.points[tpoint].positions.size() != 1 )
	{
	  ROS_ERROR_NAMED("BaxterSingleJointPositionController",
			  "BaxterSingleJointPositionController expects a joint trajectory with one \
                         single point" );
	  return false;
	}
      goal.position = (trajectory.points[tpoint].positions[0]);
      goal.max_velocity = 0.25; // HACK. Should come from speed_scalar?
      //goal.min_duration = 0.0;


      controller_action_client_->sendGoal(goal,
					  boost::bind(&BaxterSingleJointPositionControllerHandle::controllerDoneCallback, this, _1, _2),
					  boost::bind(&BaxterSingleJointPositionControllerHandle::controllerActiveCallback, this),
					  boost::bind(&BaxterSingleJointPositionControllerHandle::controllerFeedbackCallback, this, _1));
      
      done_ = false;
      last_exec_ = controller_manager_util::ExecutionStatus::RUNNING;
      return true;
  }
  
  void setCommandJoint(const std::string& name)
  {
    command_joint_ = name;
  }


private:

  void controllerDoneCallback(const actionlib::SimpleClientGoalState& state,
                              const control_msgs::SingleJointPositionResultConstPtr& result)
  {
    finishControllerExecution(state);
  }

  void controllerActiveCallback()
  {
    ROS_INFO_STREAM_NAMED("SingleJointPositionController", name_ << " started execution");
  }

  void controllerFeedbackCallback(const control_msgs::SingleJointPositionFeedbackConstPtr& feedback)
  {
  }



  /*
   * A SingleJointPosition message has only a single float64 for the
   * "position", thus only a single joint angle can be sent
   */
  std::string command_joint_;
};


} // end namespace 

