#pragma once

#include <controller_managers/simple_action_client_controller_handle.h>
#include <control_msgs/GripperCommandAction.h>
#include <set>

namespace simple_controller_manager
{

/*
 * This is an interface for a gripper using control_msgs/GripperCommandAction
 * action interface (single DOF).
 */
class BaxterGripperCommandControllerHandle :
      public SimpleActionClientControllerHandle<control_msgs::GripperCommandAction>
{
public:
  /* Topics will map to name/ns/goal, name/ns/result, etc */
  BaxterGripperCommandControllerHandle(const std::string &name, const std::string &ns) :
    SimpleActionClientControllerHandle<control_msgs::GripperCommandAction>(name, ns),
    allow_failure_(false), parallel_jaw_gripper_(false)
  {
  }

  bool sendTrajectory(const trajectory_msgs::JointTrajectory &trajectory)
  {
    ROS_INFO_STREAM_NAMED("BaxterGripperCommandController",
                           "Received new trajectory for " << name_);

    if (!controller_action_client_)
      return false;

 
    if (trajectory.points.empty())
    {
      ROS_ERROR_NAMED("BaxterGripperCommandController",
                      "BaxterGripperCommandController requires at least one joint trajectory point.");
      return false;
    }

    if (trajectory.points.size() > 1)
    {
      ROS_INFO_STREAM_NAMED("BaxterGripperCommandController","Trajectory points: " << trajectory.points.size());
    }

    if (trajectory.joint_names.empty())
    {
      ROS_ERROR_NAMED("BaxterGripperCommandController", "No joint names specified");
      return false;
    }

    std::vector<int> gripper_joint_indexes;
    for (std::size_t i = 0 ; i < trajectory.joint_names.size() ; ++i)
    {
      if (command_joints_.find(trajectory.joint_names[i]) != command_joints_.end())
      {
        gripper_joint_indexes.push_back(i);
        if (!parallel_jaw_gripper_)
          break;
      }
    }

    if (gripper_joint_indexes.empty())
    {
      ROS_WARN_NAMED("BaxterGripperCommandController",
                     "No command_joint was specified for the controller gripper handle. \
                      Please see GripperCommandControllerHandle::addCommandJoint() and \
                      GripperCommandControllerHandle::setCommandJoint(). Assuming index 0.");
      gripper_joint_indexes.push_back(0);
    }

    // goal to be sent
    control_msgs::GripperCommandGoal goal;
    goal.command.position = 0.0;
    goal.command.max_effort = 0.0;

    // send last point
    int tpoint = trajectory.points.size() - 1;
    ROS_DEBUG_NAMED("BaxterGripperCommandController",
                    "Sending command from trajectory point %d",
                    tpoint);

    // fill in goal from last point. For Baxter, fingers have oppositve signs +/- but same values. Must scale between 0 and 100 
    int idx = gripper_joint_indexes[0];
      
    if (trajectory.points[tpoint].positions.size() <= idx)
      {
        ROS_ERROR_NAMED("BaxterGripperCommandController",
                        "BaxterGripperCommandController expects a joint trajectory with one \
                         point that specifies at least the position of joint \
                         '%s', but insufficient positions provided",
                        trajectory.joint_names[idx].c_str());
        return false;
      }
    double factor = 100.0/0.021; // Gripper opens up between 0 and +/- 0.0208 
    //goal.command.position + = trajectory.points[tpoint].positions[idx];
    goal.command.position = fabs( (trajectory.points[tpoint].positions[idx])*factor );
    goal.command.max_effort = 25.0;
        
    ROS_WARN("[DEBUG] Sending Goal command position: %f \n", goal.command.position );
    controller_action_client_->sendGoal(goal,
					boost::bind(&BaxterGripperCommandControllerHandle::controllerDoneCallback, this, _1, _2),
					boost::bind(&BaxterGripperCommandControllerHandle::controllerActiveCallback, this),
					boost::bind(&BaxterGripperCommandControllerHandle::controllerFeedbackCallback, this, _1));

    done_ = false;
    last_exec_ = controller_manager_util::ExecutionStatus::RUNNING;
    return true;
  }

  void setCommandJoint(const std::string& name)
  {
    command_joints_.clear();
    addCommandJoint(name);
  }

  void addCommandJoint(const std::string& name)
  {
    command_joints_.insert(name);
  }

  void allowFailure(bool allow)
  {
    allow_failure_ = allow;
  }

  void setParallelJawGripper(const std::string& left, const std::string& right)
  {
    addCommandJoint(left);
    addCommandJoint(right);
    parallel_jaw_gripper_ = true;
  }

private:

  void controllerDoneCallback(const actionlib::SimpleClientGoalState& state,
                              const control_msgs::GripperCommandResultConstPtr& result)
  {
    if (state == actionlib::SimpleClientGoalState::ABORTED && allow_failure_)
      finishControllerExecution(actionlib::SimpleClientGoalState::SUCCEEDED);
    else
      finishControllerExecution(state);
  }

  void controllerActiveCallback()
  {
    ROS_INFO_STREAM_NAMED("GripperCommandController", name_ << " started execution");
  }

  void controllerFeedbackCallback(const control_msgs::GripperCommandFeedbackConstPtr& feedback)
  {
  }

  /*
   * Some gripper drivers may indicate a failure if they do not close all the way when
   * an object is in the gripper.
   */
  bool allow_failure_;

  /*
   * A common setup is where there are two joints that each move
   * half the overall distance. Thus, the command to the gripper
   * should be the sum of the two joint distances.
   *
   * When this is set, command_joints_ should be of size 2,
   * and the command will be the sum of the two joints.
   */
  bool parallel_jaw_gripper_;

  /*
   * A GripperCommand message has only a single float64 for the
   * "command", thus only a single joint angle can be sent -- however,
   * due to the complexity of making grippers look correct in a URDF,
   * they typically have >1 joints. The "command_joint" is the joint
   * whose position value will be sent in the GripperCommand action. A
   * set of names is provided for acceptable joint names. If any of
   * the joints specified is found, the value corresponding to that
   * joint is considered the command.
   */
  std::set<std::string> command_joints_;
};


} // end namespace 

