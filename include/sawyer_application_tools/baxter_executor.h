#pragma once

#include <execution_interface/execution_interface.h>

#include <pluginlib/class_list_macros.h>

#include <controller_manager_msgs/ListControllers.h>

#include <controller_managers/follow_joint_trajectory_control_handle.h>
#include <sawyer_application_tools/baxter_gripper_command_control_handle.h>
#include <sawyer_application_tools/baxter_single_joint_position_control_handle.h>

namespace sawyer_application_tools
{

  using namespace simple_controller_manager;

  /** @brief The base class for the moveit planner.
  * Base class to wrap the MoveIt! code API in the planner plugin architecture.
  */
  class BaxterExecutor : public execution_interface::ExecutionInterface
  {
    public:
     
      BaxterExecutor();
      ~BaxterExecutor();

    protected:
      
      bool setup_();

      bool sendTrajectory(const std::string &controller_name,
			  const trajectory_msgs::JointTrajectory &trajectory);
      
      controller_manager_util::ExecutionStatus getExecutionStatus(const std::string &controller_name);

    private:
      
      // Action client for the joint trajectory action 
      // used to trigger the arm movement action
      std::map<std::string, SimpleActionClientControllerHandleBasePtr> action_clients_;

      // client to get names of controllers and joints 
      ros::ServiceClient list_controllers_client_;

      // function call to lookup controller info from controller manager via service
      bool updateControllerInfo();

      // set up action client
      bool setupActionClient(std::string controller_name);

      void preemptMotion();

      // Function to first check if a controller_list parameter exist to load controllers,
      // if not, then use the default check to controller_manager service
      bool updated_from_controller_list_;
      bool updateControllerFromControllerList();
      // If the first function did not find a parameter, then call controller_manager service
      bool updateControllerFromDefaultService();
      
  };
};
